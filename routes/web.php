<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    if (Auth::check()) {
        return redirect('/');
    }

    return view('login');
});
Route::match(['post'], '/login', [LoginController::class, 'login'])->name('login');
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('app');
    });

    Route::get('/logout', function () {
        Auth::logout();
        session()->regenerate(true);

        return redirect('/');
    });

    Route::match(['get'], '/data', [AppController::class, 'data'])->name('data');
    Route::match(['post'], '/create', [AppController::class, 'createArchive'])->name('create');
    Route::match(['delete'], '/delete', [AppController::class, 'delete'])->name('delete');
});

