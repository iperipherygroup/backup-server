server {
    listen 62380 default_server;
    server_name _;

    auth_basic "Login to protected area";
    auth_basic_user_file /var/www/.htpasswd;

    root /var/tradingbackup/public;
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
    index index.php;
    charset utf-8;
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
    error_page 404 /index.php;
    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php8.0-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
        fastcgi_intercept_errors    on;
        fastcgi_ignore_client_abort off;
        fastcgi_connect_timeout     3s;
        fastcgi_buffer_size         128k;
        fastcgi_buffers             128 16k; # was 4 256k
        fastcgi_busy_buffers_size   256k;
        fastcgi_temp_file_write_size 256k;
        reset_timedout_connection on;
    }
   location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
                add_header Cache-Control "max-age=1800, must-revalidate";
        }

    client_body_buffer_size 1m; # was 10K
    client_header_buffer_size 1k;
    large_client_header_buffers 4 16k;
    keepalive_timeout 2 2;
    proxy_http_version 1.1;
    proxy_set_header Connection "";

    gzip on;
    gzip_disable "msie6";
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 9;
    gzip_min_length 1100;
    gzip_buffers 16 8k;
    gzip_http_version 1.1;
}
