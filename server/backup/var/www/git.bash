#!/bin/bash

usage() { echo "Usage: $0 --rlist rlist --spath path" 1>&2; exit 1; }

if [ "$#" !=  "4" ]
    then
         usage
fi

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --rlist) rlist="$2"; shift ;;
        --spath) store_path="$2"; shift ;;
        *) usage ;;
    esac
    shift
done

repository=""

fetchRepository() {
    if [[ $repository =~ :(.+/.+)(\.git)?$ ]];
    then
        repositoryPath=$store_path/${BASH_REMATCH[1]}
        path=$(pwd)

        if [ -d $repositoryPath ];
        then
            echo "Fetching repository $repository in path $repositoryPath"
            cd $repositoryPath && git fetch
        else
            echo "Cloning repository $repository"
            mkdir -p $repositoryPath
            cd $repositoryPath && git clone $repository .
        fi

        cd $path
    fi
}

fetchGit() {
    echo "fetching repositories"
    mkdir -p $store_path

    while read repository; do
      repository=$repository
      fetchRepository
    done <$rlist
}

fetchGit
