#!/bin/bash

usage() { echo "Usage: $0 --path path --pattern pattern" 1>&2; exit 1; }


if [ "$#" !=  "4" ]
    then
         usage
fi

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --path) path="$2"; shift ;;
        --pattern) pattern="$2"; shift ;;
        *) usage ;;
    esac
    shift
done

rotateAppBackups() {
    rotateBefore=$(date --date="8 days ago")
    files=$(find $appPath -type f ! -newermt "$rotateBefore" | grep $appPattern | sort)
    for file in $files
    do
        dayOfWeekName=$(date -d @$(stat -c %Y $file) +%a)

        if [ "$dayOfWeekName" != "Sun" ]; then
            #echo $file
            rm -f $file
        fi
    done
}

rotate() {
    echo "rotate files"
    rotateBefore=$(date --date="8 days ago")
    files=$(find $path -type f -name $pattern ! -newermt "$rotateBefore"  | sort)
    declare -a allDates
    counter=0

    for file in $files
    do
        allDates[$counter]=$(date -d @$(stat -c %Y $file) +%Y-%m-%d)
        counter=$((counter+1))
    done

    for date in $(printf '%s\n' "${allDates[@]}"|sort -u)
    do
        loop=0

        for file in $(find $path -type f -name $pattern -newermt "$date 00:00:00" ! -newermt "$date 23:59:59"  | sort)
        do
            if [ $loop != 0 ]; then
                rm -f $file
            fi

            loop=$((loop+1))
        done
    done
}

rotate
