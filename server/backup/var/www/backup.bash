#!/bin/bash

usage() { echo "Usage: $0 --ssh-ip ip --ssh-login login --rpath path --suffix suffix --spath path" 1>&2; exit 1; }

if [ "$#" !=  "10" ]
    then
         usage
fi

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --ssh-ip) ssh_ip="$2"; shift ;;
        --ssh-login) ssh_login="$2"; shift ;;
        --rpath) remote_path="$2"; shift ;;
        --suffix) suffix="$2"; shift ;;
        --spath) store_path="$2"; shift ;;
        *) usage ;;
    esac
    shift
done

backupFiles() {
    echo "backup files"
    mkdir -p $store_path
    path=/var/$(date +%Y-%m-%d_%H:%M:%S)_$suffix.zip
    ssh $ssh_login@$ssh_ip zip -r -6 $path $remote_path
    scp $ssh_login@$ssh_ip:$path $store_path
    ssh $ssh_login@$ssh_ip rm -f $path
}

backupFiles
