#!/bin/bash

usage() { echo "Usage: $0 --ssh-ip ip --ssh-login login --rpath path --spath path" 1>&2; exit 1; }


if [ "$#" !=  "8" ]
    then
         usage
fi

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --ssh-ip) ssh_ip="$2"; shift ;;
        --ssh-login) ssh_login="$2"; shift ;;
        --rpath) remote_path="$2"; shift ;;
        --spath) store_path="$2"; shift ;;
        *) usage ;;
    esac
    shift
done

syncFiles() {
    echo "syncing files"
    mkdir -p $store_path
    scp -r $ssh_login@$ssh_ip:$remote_path/* $store_path
}

syncFiles
