#!/bin/bash

usage() { echo "Usage: $0 --path path --pattern pattern" 1>&2; exit 1; }


if [ "$#" !=  "4" ]
    then
         usage
fi

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --path) path="$2"; shift ;;
        --pattern) pattern="$2"; shift ;;
        *) usage ;;
    esac
    shift
done

rotate() {
    rotateBefore=$(date --date="8 days ago")
    files=$(find $path -type f ! -newermt "$rotateBefore" -name $pattern | sort)
    for file in $files
    do
        dayOfWeekName=$(date -d @$(stat -c %Y $file) +%a)

        if [ "$dayOfWeekName" != "Sun" ]; then
            rm -f $file
        fi
    done
}

rotate
