apt update
apt install sshpass zip unzip git
timedatectl set-timezone UTC

ssh-keygen
(now add .id_rsa.pub to gitlab server)
mkdir /var/git

--backup scripts--
mkdir /var/www
(see scripts in server/backup/var/www/new)
crontab -e  
(put next --
*/30 * * * * cd /var/www/ && bash sync.bash --ssh-ip 104.219.251.120 --ssh-login root --rpath /var/www/imaingroup/backups --spath /var/www/backup_main/
*/30 * * * * cd /var/www/ && bash backup.bash --ssh-ip 104.219.251.120 --ssh-login root --rpath /var/www/imaingroup/secrets --suffix secrets --spath /var/www/backup_main/
*/30 * * * * cd /var/www/ && bash backup.bash --ssh-ip 104.219.251.120 --ssh-login root --rpath /var/www/imaingroup/data_main/logs/backend --suffix app_logs --spath /var/www/backup_main/
0 0 * * * cd /var/www/ && bash backup.bash --ssh-ip 104.219.251.120 --ssh-login root --rpath /var/www/imaingroup/data_main/app/storage --suffix storage --spath /var/www/backup_main/

*/5 * * * * cd /var/www/ && bash backup.bash --ssh-ip 144.76.60.184 --ssh-login root --rpath /var/www/iecgroup/data_main/geth/data/keystore --suffix wallets --spath /var/www/backup_eth/
*/5 * * * * cd /var/www/ && bash sync.bash --ssh-ip 104.219.251.120 --ssh-login root --rpath /var/www/imaingroup/data_main/logs/backend --spath /var/www/prod_logs_sync/

0 0 * * * cd /var/www/ && bash backup.bash --ssh-ip 104.219.250.132 --ssh-login root --rpath /root/.bitcoin/wallets/ --suffix wallets --spath /var/www/backup_btc/

*/30 * * * * cd /var/www/ && bash sync.bash --ssh-ip 63.250.41.2 --ssh-login root --rpath /var/www/ibotgroup/backups --spath /var/www/backup_bot/
*/30 * * * * cd /var/www/ && bash sync.bash --ssh-ip 63.250.41.2 --ssh-login root --rpath /var/www/itest-group/backups --spath /var/www/backup_redmine/
0 0 * * * cd /var/www/ && bash backup.bash --ssh-ip 63.250.41.2 --ssh-login root --rpath /var/www/itest-group/data_main/redmine --suffix files --spath /var/www/backup_redmine/

*/30 * * * * cd /var/www/ && bash git.bash --rlist /var/www/repositories --spath /var/www/git

0 0 * * * cd /var/www/ && bash backup.bash --ssh-ip 66.29.138.190 --ssh-login root --rpath /var/iblockchaingroup/data_main --suffix data --spath /var/www/backup_blockhain/
0 0 * * * cd /var/www/ && bash backup.bash --ssh-ip 66.29.138.190 --ssh-login root --rpath /var/iblockchaingroup/secrets --suffix secrets --spath /var/www/backup_blockhain/


*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_main/ --pattern *.sql.gz
*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_main/ --pattern *_secrets.zip
*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_main/ --pattern *_app_logs.zip
*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_main/ --pattern *_storage.zip
*/30 * * * * cd /var/www/ && bash rotateWeekly.bash --path /var/www/backup_main/ --pattern *_storage.zip

*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_eth/ --pattern *_wallets.zip
*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_btc/ --pattern *_wallets.zip

*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_bot/ --pattern *.sql.gz

*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_redmine/ --pattern *.sql.gz
*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_redmine/ --pattern *_files.zip

*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_blockhain/ --pattern *_data.zip
*/30 * * * * cd /var/www/ && bash rotate.bash --path /var/www/backup_blockhain/ --pattern *_secrets.zip
*/30 * * * * cd /var/www/ && bash rotateWeekly.bash --path /var/www/backup_blockhain/ --pattern *_data.zip
--)



---install download interface---
apt install software-properties-common
add-apt-repository ppa:ondrej/php
apt update
apt install php8.0 php8.0-fpm php8.0-bcmath php8.0-curl php8.0-zip php8.0-mbstring php8.0-mysql php8.0-xml nginx mariadb-server git zip unzip apache2-utils
mysql_secure_installation
mysql
(execute next sql queries: «CREATE DATABASE backup CHARACTER SET utf8 COLLATE utf8_general_ci», «GRANT ALL PRIVILEGES ON backup.* TO 'backup'@'localhost' IDENTIFIED BY '{password}'», «FLUSH PRIVILEGES»)
exit
htpasswd -c /var/www/.htpasswd {USERNAME}
nano /etc/nginx/sites-available/default
(put text from file server/backup/etc/nginx/sites-available/default and replace ip address in config.)
systemctl restart nginx
cd /var/
git clone https://gitlab.com/btc_trading/tradingbackup
(now need install composer2, see it https://getcomposer.org/download/
need install composer to /usr/bin/composer)
chown -R www-data:www-data /var/tradingbackup/  
cd /var/www/tradingbackup/
su www-data -s /bin/sh
composer install
php artisan storage:link
cp .env.example .env
nano .env
(set mysql login/password/db, set debug=false, set url, set a backup path)
php artisan key:generate
php artisan migrate
php artisan user:create {username} {password}
(after it write g2fa code, zip password.)
crontab -e
(from user www-data, insert line «* * * * * cd /var/tradingbackup && php artisan schedule:run >> /dev/null 2>&1»)
exit
