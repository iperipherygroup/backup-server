<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Service\UserService;

final class LoginController extends Controller
{
    protected function login(LoginRequest $request)
    {
        $this->getUserService()->login(...$request->validated());
    }

    /**
     * Get UserService.
     *
     * @return UserService
     */
    private function getUserService(): UserService
    {
        return app(UserService::class);
    }
}
