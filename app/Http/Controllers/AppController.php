<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\DeleteFileException;
use App\Http\Requests\CreateRequest;
use App\Http\Requests\DataRequest;
use App\Http\Requests\DeleteRequest;
use App\Jobs\CreateArchiveJob;
use App\Service\AppService;
use App\Service\ArchiveService;
use JetBrains\PhpStorm\ArrayShape;

/**
 * AppController.
 */
final class AppController extends Controller
{
    /**
     * Get data.
     *
     * @param DataRequest $request
     * @return array
     */
    #[ArrayShape(['files' => "array", 'logins' => "array"])]
    protected function data(DataRequest $request): array
    {
        return $this->getAppService()
            ->getData();
    }

    /**
     * Create archive.
     *
     * @param CreateRequest $request
     * @return array
     */
    #[ArrayShape(['status' => "bool"])]
    protected function createArchive(CreateRequest $request): array
    {
        CreateArchiveJob::dispatch();

        return ['status' => true];
    }

    /**
     * Delete file.
     *
     * @param DeleteRequest $request
     * @return array
     * @throws DeleteFileException
     */
    #[ArrayShape(['status' => "bool"])]
    protected function delete(DeleteRequest $request): array
    {
        $this->getArchiveService()->delete($request->validated()['file']);

        return ['status' => true];
    }

    /**
     * Get ArchiveService.
     *
     * @return ArchiveService
     */
    private function getArchiveService(): ArchiveService
    {
        return app(ArchiveService::class);
    }

    /**
     * Get AppService.
     *
     * @return AppService
     */
    private function getAppService(): AppService
    {
        return app(AppService::class);
    }
}
