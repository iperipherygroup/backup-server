<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Repository\UserRepository;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use JetBrains\PhpStorm\ArrayShape;

/**
 * This request class need for validating data from register form.
 */
final class LoginRequest extends FormRequest
{
    /**
     * Constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(
        private UserRepository $userRepository,
    )
    {
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return !Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['email' => "string", 'password' => "string", 'g2fa' => "string"])]
    public function rules(): array
    {
        return [
            'email' => 'required|string|exists:users,email',
            'password' => 'required|scalar',
            'g2fa' => 'required|scalar|g2fa',
        ];
    }

    /**
     * {@inheritdoc}
     */
    #[ArrayShape(['email' => "string", 'password' => "string"])]
    public function validated(): array
    {
        return [
            'email' => $this['email'],
            'password' => $this['password'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function getValidatorInstance(): Validator
    {
        return parent::getValidatorInstance()->after(function (Validator $validator) {
            if (count($validator->failed()) === 0) {
                $this->after($validator);
            }
        });
    }

    /**
     * Post validation method.
     *
     * @param Validator $validator
     * @return void
     */
    private function after(Validator $validator)
    {
        $user = $this->userRepository->findByEmail($this['email']);

        if (!Hash::check($this['password'], $user->password)) {
            $validator->errors()->add('password', 'Email or password is incorrect');
            return;
        }
    }
}
