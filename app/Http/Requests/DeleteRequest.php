<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\Service\ArchiveService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use JetBrains\PhpStorm\ArrayShape;

/**
 * DeleteRequest.
 */
final class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    #[ArrayShape(['file' => "string"])]
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                Rule::in(
                    collect(
                        $this->getArchiveService()
                            ->list()
                    )->pluck('file')
                )
            ]
        ];
    }

    /**
     * Get ArchiveService.
     *
     * @return ArchiveService
     */
    private function getArchiveService(): ArchiveService
    {
        return app(ArchiveService::class);
    }
}
