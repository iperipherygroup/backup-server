<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * CreateArchiveException.
 */
final class CreateArchiveException extends Exception
{
}
