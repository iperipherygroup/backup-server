<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * DeleteFileException.
 */
final class DeleteFileException extends Exception
{
}
