<?php
declare(strict_types=1);

namespace App\Exceptions;

/**
 * LoginException.
 */
final class LoginException extends \Exception
{
}
