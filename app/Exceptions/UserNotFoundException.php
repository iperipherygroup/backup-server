<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * UserNotFoundException.
 */
final class UserNotFoundException extends Exception
{
}
