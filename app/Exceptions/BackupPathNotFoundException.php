<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * BackupPathNotFoundException.
 */
final class BackupPathNotFoundException extends Exception
{
}
