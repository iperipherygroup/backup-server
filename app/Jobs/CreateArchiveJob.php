<?php
declare(strict_types=1);

namespace App\Jobs;

use App\Exceptions\BackupPathNotFoundException;
use App\Exceptions\CreateArchiveException;
use App\Exceptions\UserNotFoundException;
use App\Service\ArchiveService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

/**
 * Create archive job.
 */
class CreateArchiveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the job.
     *
     * @throws BackupPathNotFoundException
     * @throws CreateArchiveException
     * @throws UserNotFoundException
     * @throws Throwable
     */
    public function handle(): void
    {
        $this->getArchiveService()
            ->create();
    }

    /**
     * Get ArchiveService
     *
     * @return ArchiveService
     */
    private function getArchiveService(): ArchiveService
    {
        return app(ArchiveService::class);
    }
}
