<?php
declare(strict_types=1);

namespace App\Validation;

use App\Repository\UserRepository;
use App\Service\UserService;
use Illuminate\Validation\Validator;
use Throwable;

/**
 * Validate that 2fa is correct.
 */
final class G2Fa
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        if (!isset($validator->getData()['email']) || !is_string($validator->getData()['email'])) {
            return false;
        }

        $user = $this->getUserRepository()->findByEmail($validator->getData()['email']);

        if(!$user || !$user->g2fa_secret) {
            return false;
        }

        if (!is_string($value)) {
            return false;
        }

        try {
            if (!$this->getUserService()->check2fa($user, $value)) {
                return false;
            }
        } catch (Throwable $e) {
            return false;
        }

        return true;
    }

    /**
     * Get UserRepository.
     *
     * @return UserRepository
     */
    private function getUserRepository(): UserRepository
    {
        return app(UserRepository::class);
    }

    /**
     * Get UserService.
     *
     * @return UserService
     */
    private function getUserService(): UserService
    {
        return app(UserService::class);
    }
}
