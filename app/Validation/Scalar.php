<?php
declare(strict_types=1);

namespace App\Validation;


use Illuminate\Validation\Validator;

/**
 * This class validating type is scalar value.
 */
final class Scalar
{
    /**
     * Validate.
     *
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function validate(string $attribute, mixed $value, array $parameters, Validator $validator): bool
    {
        return is_scalar($value);
    }
}
