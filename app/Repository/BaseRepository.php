<?php
declare(strict_types=1);

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Base repository.
 */
abstract class BaseRepository implements EloquentRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function create(array $attributes): Model
    {
        $class = \get_class($this->model);
        $model = new $class();

        foreach ($attributes as $key => $value) {
            $model->$key = $value;
        }

        $model->save();

        return $model;
    }

    /**
     * {@inheritDoc}
     */
    public function find(mixed $id): ?Model
    {
        return $this->model->find($id);
    }

    /**
     * Find by sid.
     *
     * @param int $sid
     * @return Model|null
     */
    public function findBySid(mixed $sid): ?Model
    {
        return $this->model
            ->newQuery()
            ->where('sid', $sid)
            ->first();
    }

    /**
     * Get all data.
     *
     * @param array $exclude
     * @param string $orderBy
     * @param string $orderType
     * @param bool $lockForUpdate
     * @return Collection
     */
    public function all(array $exclude = [], string $orderBy = 'id', string $orderType = 'ASC', bool $lockForUpdate = false): Collection
    {
        $query = $this->model->newQuery()
            ->orderBy($orderBy, $orderType);

        if (count($exclude) > 0) {
            $query->whereNotIn('id', $exclude);
        }

        if ($lockForUpdate) {
            $query->lockForUpdate();
        }

        return $query->get();
    }

    /**
     * Get by ids.
     *
     * @param Collection|array $ids
     * @return Collection
     */
    public function getByIds(Collection|array $ids): Collection
    {
        if (count($ids) === 0) {
            return collect();
        }

        return $this->model
            ->newQuery()
            ->whereIn('id', $ids)
            ->get();
    }

    /**
     * Count all models.
     *
     * @return int
     */
    public function count(): int
    {
        return $this->model
            ->newQuery()
            ->count();
    }

    /**
     * Get models with limit and offset.
     *
     * @param int $limit
     * @param int $offset
     * @param bool $lockForUpdate
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getWithLimitOffset(int $limit, int $offset, bool $lockForUpdate = false): \Illuminate\Database\Eloquent\Collection
    {
        $query = $this->model
            ->newQuery()
            ->orderBy('id', 'ASC')
            ->limit($limit)
            ->offset($offset);

        if ($lockForUpdate) {
            $query->lockForUpdate();
        }

        return $query->get();
    }

    /**
     * Get models with limit and offset.
     *
     * @param int $id
     * @return Model|null
     */
    public function getAndLockForUpdate(int $id): ?Model
    {
        return $this->model
            ->newQuery()
            ->where('id', $id)
            ->lockForUpdate()
            ->first();
    }
}
