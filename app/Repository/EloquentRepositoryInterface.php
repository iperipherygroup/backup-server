<?php
declare(strict_types=1);

namespace App\Repository;


use Illuminate\Database\Eloquent\Model;

/**
 * Repositories interface.
 */
interface EloquentRepositoryInterface
{
    /**
     * Create object.
     *
     * @param array $attributes
     *
     * @return Model
     */
    public function create(array $attributes): Model;

    /**
     * Fin by ID.
     *
     * @param int $id
     * @return Model|null
     */
    public function find(int $id): ?Model;
}
