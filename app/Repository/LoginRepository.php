<?php
declare(strict_types=1);


namespace App\Repository;

use App\Models\Login;

final class LoginRepository extends BaseRepository
{
    /**
     * Constructor..
     *
     * @param Login $model
     */
    public function __construct(protected Login $model)
    {
    }
}
