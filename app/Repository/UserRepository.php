<?php
declare(strict_types=1);


namespace App\Repository;

use App\Models\User;

final class UserRepository extends BaseRepository
{
    /**
     * Constructor..
     *
     * @param User $model
     */
    public function __construct(protected User $model)
    {
    }

    public function findByEmail(string $email): ?User
    {
        return $this->model->newQuery()
            ->where('email', $email)
            ->first();
    }
}
