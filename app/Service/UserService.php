<?php
declare(strict_types=1);

namespace App\Service;

use App\Exceptions\LoginException;
use App\Models\User;
use App\Repository\LoginRepository;
use Illuminate\Support\Facades\Auth;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FALaravel\Google2FA;

/**
 * User service.
 */
final class UserService
{
    /**
     * Check 2fa.
     *
     * @param User $user
     * @param string $code
     * @param string|null $secret
     * @return mixed
     * @throws SecretKeyTooShortException
     */
    public function check2fa(User $user, string $code, string $secret = null)
    {

        $string2fa = trim(str_replace(' ', '', $code));

        /** @var Google2FA $google2fa */
        $google2fa = app('pragmarx.google2fa');

        return $google2fa->verifyGoogle2FA($secret ? $secret : $user->g2fa_secret, $string2fa);
    }

    /**
     * Login.
     *
     * @param string $email
     * @param string $password
     * @return User
     * @throws LoginException
     */
    public function login(string $email, string $password): User
    {
        if (!Auth::attempt(['email' => $email, 'password' => $password], false)) {
            throw new LoginException();
        }

        $user = Auth::user();

        $this->getLoginRepository()->create([
            'ip' => request()->getClientIp(),
            'users_id' => $user->id,
        ]);

        return $user;
    }

    /**
     * Get LoginRepository.
     *
     * @return LoginRepository
     */
    private function getLoginRepository(): LoginRepository
    {
        return app(LoginRepository::class);
    }
}
