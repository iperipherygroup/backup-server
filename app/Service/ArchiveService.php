<?php
declare(strict_types=1);

namespace App\Service;

use App\Exceptions\BackupPathNotFoundException;
use App\Exceptions\CreateArchiveException;
use App\Exceptions\DeleteFileException;
use App\Exceptions\UserNotFoundException;
use App\Models\User;
use DateTime;
use Exception;
use Illuminate\Support\Facades\Storage;
use Throwable;
use ZipArchive;

/**
 * Archive service.
 */
final class ArchiveService
{
    /**
     * List.
     */
    public function list(): array
    {
        $files = array_filter(
            Storage::disk('public')->files(),
            function (string $file) {
                return $file !== '.gitignore';
            }
        );

        $res = array_map(
            function (string $file) {
                $dt = new DateTime();
                $dt->setTimestamp(Storage::disk('public')->lastModified($file));

                return [
                    'file' => $file,
                    'url' => Storage::disk('public')->url($file),
                    'size' => Storage::disk('public')->size($file),
                    'last_modified' => $dt,
                ];
            },
            $files
        );

        $res = collect($res);

        return $res->sortByDesc('last_modified')->values()->toArray();
    }

    /**
     * Create.
     *
     * @throws BackupPathNotFoundException
     * @throws UserNotFoundException
     * @throws CreateArchiveException
     * @throws Throwable
     */
    public function create()
    {
        $zipPassword = $this->getZipPassword();
        $path = $this->getBasePath();
        $files = [];
        $throw = null;

        foreach (scandir($path) as $item) {
            try {
                if (!in_array($item, ['.', '..'])) {
                    $itemPath = sprintf('%s/%s/*', $path, $item);
                    $search = glob($itemPath);

                    if ($search === false) {
                        continue;
                    }

                    foreach ($this->getTypes($search) as $type) {
                        $files[] = $this->getLastFileByType($search, $type);
                    }
                }
            } catch (Throwable $e) {
                $throw = $e;
            }
        }

        $string = $this->createArchive($files, $zipPassword);

        return $throw ? throw $throw : $string;
    }

    /**
     * Delete file.
     *
     * @param string $file
     * @throws DeleteFileException
     */
    public function delete(string $file): void
    {
        if(!Storage::disk('public')->delete($file)) {
            throw new DeleteFileException();
        }
    }

    /**
     * Create archive.
     *
     * @param array $files
     * @param string $zipPassword
     * @return string
     * @throws CreateArchiveException
     */
    private function createArchive(array $files, string $zipPassword): string
    {
        $path = sprintf(
            '%s/%s.zip',
            config('filesystems.disks.public.root'),
            (new DateTime())->format('Y-m-d_H:i:s')
        );

        $zip = new ZipArchive();

        if ($zip->open($path, ZipArchive::CREATE | ZipArchive::CHECKCONS) !== TRUE) {
            throw new CreateArchiveException();
        }

        $zip->setPassword($zipPassword);
        $index = 0;

        foreach ($files as $file) {
            $zip->addFile($file);
            $zip->setEncryptionName($file, ZipArchive::EM_AES_256);
            $zip->setCompressionIndex($index, ZipArchive::CM_DEFLATE64);

            $index++;
        }

        $zip->close();

        return $path;
    }

    /**
     * Get last file by type.
     *
     * @param array $files
     * @param string $type
     * @return string|null
     * @throws Exception
     */
    private function getLastFileByType(array $files, string $type): ?string
    {
        $list = [];

        foreach ($files as $file) {
            if (preg_match(sprintf('/^.*%s$/', $type), $file)) {
                $list[$file] = $this->extractDate($file);
            }
        }

        asort($list);
        $keys = array_keys($list);

        return count($keys) > 0 ? $keys[count($keys) - 1] : null;
    }

    /**
     * Extract date.
     *
     * @param string $file
     * @return DateTime|null
     * @throws Exception
     */
    private function extractDate(string $file): ?DateTime
    {
        if (preg_match('/^.*(\d{4}-\d{2}-\d{2})[_-](\\d{1,2}:\\d{1,2}:\\d{1,2}).*$/', $file, $matches)) {
            return new DateTime(sprintf('%s %s', $matches[1], $matches[2]));
        }

        return null;
    }

    /**
     * Get types.
     *
     * @param array $files
     * @return array
     */
    private function getTypes(array $files): array
    {
        $types = [];

        foreach ($files as $filesItem) {
            if (preg_match('/^.+\d{4}-\d{2}-\d{2}[_-]\\d{1,2}:\\d{1,2}:\\d{1,2}(.+)$/', $filesItem, $matches)) {
                $types[] = $matches[1];
            }
        }

        return array_unique($types);
    }

    /**
     * Get base path.
     *
     * @return string
     * @throws BackupPathNotFoundException
     */
    private function getBasePath(): string
    {
        $path = env('BACKUPS_PATH');

        if (!$path || !file_exists($path)) {
            throw new BackupPathNotFoundException();
        }

        return $path;
    }

    /**
     * Get LoginRepository.
     *
     * @throws UserNotFoundException
     */
    private function getZipPassword(): string
    {
        $user = User::query()
            ->orderBy('id', 'ASC')
            ->first();

        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user->zip_password;
    }
}
