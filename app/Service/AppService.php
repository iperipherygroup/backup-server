<?php
declare(strict_types=1);

namespace App\Service;

use App\Repository\LoginRepository;
use Illuminate\Support\Facades\DB;
use JetBrains\PhpStorm\ArrayShape;

/**
 * App service.
 */
final class AppService
{
    /**
     * Get data.
     *
     * @return array
     */
    #[ArrayShape([
        'queue' => "array",
        'files' => "array",
        'logins' => "array",
    ])]
    public function getData(): array
    {
        return [
            'queue' => $this->getQueueData(),
            'files' => $this->getArchiveService()
                ->list(),
            'logins' => $this->getLoginsData(),
            'test' => collect($this->getArchiveService()
                ->list())->pluck('file')
        ];
    }

    /**
     * Get queue data.
     *
     * @return array
     */
    #[ArrayShape(['new' => "int", 'creating' => "int", 'errors' => "int"])]
    private function getQueueData(): array
    {
        return [
            'new' => DB::table('jobs')->where('attempts', 0)->count(),
            'creating' => DB::table('jobs')->where('attempts', '>', 0)->count(),
            'errors' => DB::table('failed_jobs')->count(),
        ];
    }

    /**
     * Get logins data.
     *
     * @return array
     */
    private function getLoginsData(): array
    {
        $data = [];

        foreach ($this->getLoginRepository()->all() as $login) {
            $item = [];
            $item['id'] = $login->users_id ?? 'N/A';
            $item['email'] = $login->user->email ?? 'N/A';
            $item['ip'] = $login->ip;
            $item['login_at'] = $login->created_at->format('Y-m-d H:i:s');

            $data[] = $item;
        }

        return $data;
    }

    /**
     * Get LoginRepository.
     *
     * @return LoginRepository
     */
    private function getLoginRepository(): LoginRepository
    {
        return app(LoginRepository::class);
    }

    /**
     * Get ArchiveService.
     *
     * @return ArchiveService
     */
    private function getArchiveService(): ArchiveService
    {
        return app(ArchiveService::class);
    }
}
