<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Models\User;
use App\Repository\UserRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException;
use PragmaRX\Google2FA\Exceptions\InvalidCharactersException;
use PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException;
use PragmaRX\Google2FALaravel\Google2FA;
use function sprintf;

/**
 * CreateUserCommand.
 */
final class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command making user';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws IncompatibleWithGoogleAuthenticatorException
     * @throws InvalidCharactersException
     * @throws SecretKeyTooShortException
     */
    public function handle(): int
    {
        if (User::query()->count() > 0) {
            return -1;
        }

        $user = trim($this->argument('email'));
        $password = trim($this->argument('password'));

        /** @var Google2FA $google2fa */
        $google2fa = app('pragmarx.google2fa');
        $google2faSecret = $google2fa->generateSecretKey();
        $zipPassword = Str::random(64);

        $this->getUserRepository()->create([
            'name' => $user,
            'email' => $user,
            'zip_password' => $zipPassword,
            'password' => bcrypt($password),
            'g2fa_secret' => $google2faSecret
        ]);

        $this->info(sprintf('Created. G2fa: "%s". Zip password: "%s"', $google2faSecret, $zipPassword));

        return 0;
    }

    /**
     * Get UserRepository.
     *
     * @return UserRepository
     */
    private function getUserRepository(): UserRepository
    {
        return app(UserRepository::class);
    }
}
