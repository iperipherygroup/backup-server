<?php
declare(strict_types=1);

namespace App\Console\Commands;

use App\Service\ArchiveService;
use Illuminate\Console\Command;
use Throwable;

/**
 * CreateBackupCommand.
 */
final class CreateBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command making backup';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws Throwable
     */
    public function handle(): int
    {
        $this->getArchiveService()->create();

        return 0;
    }

    /**
     * Get ArchiveService.
     *
     * @return ArchiveService
     */
    private function getArchiveService(): ArchiveService
    {
        return app(ArchiveService::class);
    }
}
